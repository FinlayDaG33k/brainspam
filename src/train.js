/**
 * Import required libraries
 */
const brain = require('brain.js');
const fs = require('fs').promises;

/**
 * Import utility functions
 */
const msToTime = require('./util/msToTime').msToTime;
const writeLog = require('./util/writeLog').writeLog;
const asyncForEach = require('./util/asyncForEach').asyncForEach;

/**
 * Define out variables
 */
const settings = {
  iterations: 5, // Iterations to train for
  learningRate: 0.3, // Rate of learning 0 -> 1, higher == faster == worse accuracy
};
var net = new brain.NeuralNetworkGPU(); // Will contain our network later
const trainingData = []; // Our training data
const folders = { // Our datasets
  spam: {
    folder: './datasets/prepared/spam/'
  },
  legit: {
    folder: './datasets/prepared/legit/'
  }
};
let timings = {
  script: new Date(), // The time we started this script
  train: null // The time we started training
};

/**
 * Scan the folders
 */
const loadDataset = async (dataset) => {
  // Get a list of all the files
  var files = await fs.readdir(folders[dataset].folder);

  // Loop over each file to process it
  await asyncForEach(files, async (file) => {
    // Load the content of the file
    let content = await fs.readFile(folders[dataset].folder + file, 'utf8');

    // Push the content into our trainingData
    trainingData.push({
      input: content,
      output: dataset
    });
  });
}

async function train() {
  // Load the datasets
  writeLog(`Loading dataset for spam...`);
  await loadDataset('spam');
  writeLog(`Loading dataset for legit...`);
  await loadDataset('legit');

  // Inform the user loading is completed
  timings.train = new Date();
  writeLog(`Finished loading all data (took ${msToTime(timings.train - timings.script)} for ${trainingData.length} items)`);

  // Start training
  writeLog(`Starting training for ${settings.iterations} iterations`);
  net.train(
    trainingData,
    { 
      iterations: settings.iterations,
      callback: function(prog) {
        const currentTime = new Date();
        writeLog(`Finished iteration #${prog.iterations + 1}/${settings.iterations} with an error of ${prog.error} (${msToTime((currentTime - timings.train) / (prog.iterations + 1))} per iteration)`);
      },
      callbackPeriod: 1,
      learningRate: settings.learningRate
    }
  );

  const currentTime = new Date();
  writeLog(`Finished training with ${settings.iterations} (took ${msToTime(currentTime - timings.train)} - ${msToTime(currentTime-timings.script)} total})`);
  writeLog(`writing results to "net.json"`);
  fs.writeFile("./net.json", JSON.stringify(net))
    .then(function() {
      writeLog(`Goodbye!`);
    });
}

train();
