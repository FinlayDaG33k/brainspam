module.exports.writeLog = function(msg, nato = false) {
  const currentTime = new Date(),
        year = currentTime.getFullYear(),
        month = currentTime.getMonth(),
        day = currentTime.getDate(),
        hour = currentTime.getHours(),
        minute = currentTime.getMinutes(),
        second = currentTime.getSeconds()
  ;
  
  let timeNice;
  if(nato) {
    timeNice = `${day} ${hour}${minute}Z ${month} ${year}`;
  } else {
    timeNice = `${day}/${month}/${year} ${hour}:${minute}:${second}`;
  }
    
  console.log(`[${timeNice}] ${msg}`);
}