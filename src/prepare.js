/**
 * Import required libraries
 */
const fs = require('fs').promises;
const simpleParser = require('mailparser').simpleParser;
const crypto = require('crypto');

/**
 * Import utility functions
 */
const msToTime = require('./util/msToTime').msToTime;
const writeLog = require('./util/writeLog').writeLog;
const asyncForEach = require('./util/asyncForEach').asyncForEach;

const folders = { // Our datasets
  spam: {
    folder: './datasets/raw/spam/'
  },
  legit: {
    folder: './datasets/raw/legit/'
  }
};

async function prepareDataset(dataset) {
  // Get a list of all the files
  var files = await fs.readdir(folders[dataset].folder);

  // Loop over each file to process it
  await asyncForEach(files, async (file) => {
    // Load the content of the file
    let content = await fs.readFile(folders[dataset].folder + file, 'utf8');

    // Parse the content of the file
    let parsed = await simpleParser(content);

    // Get the text body of the file
    let body = parsed.text;

    if(body === undefined) {
      writeLog(`File \"${dataset}/${file}\" is invalid, skipping it`);
      return;
    }

    // Check if the body already exists
    let hash = crypto.createHash('sha256').update(body, 'utf8').digest('hex');

    // Save the file
    fs.writeFile(`./datasets/prepared/${dataset}/${hash}.txt`, body)
      .then(function() {
        writeLog(`Saved file "${hash}.txt" to the dataset "${dataset}"`);
      });
  });
}

async function prepare() {
  await prepareDataset('spam');
}

prepare();